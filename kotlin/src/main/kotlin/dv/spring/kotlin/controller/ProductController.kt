package dv.spring.kotlin.controller

import dv.spring.kotlin.service.ProductService
import dv.spring.kotlin.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
class ProductController{
    @Autowired
    lateinit var productService: ProductService
    @GetMapping("/product")
//    fun getAllProduct(): ResponseEntity<Any>{
//        val products = productService.getProducts()
//        val productDtos = mutableListOf<ProductDto>()
//        for (product in products){
//            productDtos.add(ProductDto(product.name,
//                    product.description,
//                    product.price,
//                    product.amountInStock,
//                    product.imageUrl))
//        }
//        return ResponseEntity.ok(productDtos)
//    }
    fun getProduct():ResponseEntity<Any>{
        val products = productService.getProducts()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapProductDto(products))
    }
//    @GetMapping("/product/query")
//    fun getProducts (@RequestParam("name")name:String):ResponseEntity<Any>{
//        return  ResponseEntity.ok(MapperUtil.INSTANCE.mapProductDto(productService.getProductByName(name)))
//    }
}