package dv.spring.kotlin.controller


import dv.spring.kotlin.service.ShoppingCartService
import dv.spring.kotlin.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class ShoppingCartController {
    @Autowired
    lateinit var shoppingCartService: ShoppingCartService

    @GetMapping("/shoppingcart")
    fun getAllShoppingCart(): ResponseEntity<Any> {
        val shoppingCart = shoppingCartService.getShoppingCarts()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapShoppingCartDto(shoppingCart))
    }
}