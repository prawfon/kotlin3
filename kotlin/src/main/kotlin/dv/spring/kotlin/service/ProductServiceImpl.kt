package dv.spring.kotlin.service

import dv.spring.kotlin.dao.ProductDao
import dv.spring.kotlin.entity.Product
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class ProductServiceImpl: ProductService{
    @Autowired
    lateinit var productDao: ProductDao
//    override fun getProductByName(name: String): Product
//    = productDao.getProductByName(name)

    override fun getProducts(): List<Product> {
        return productDao.getProducts()
    }
}