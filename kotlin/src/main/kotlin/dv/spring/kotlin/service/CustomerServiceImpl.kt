package dv.spring.kotlin.service

import dv.spring.kotlin.dao.CustomerDao
import dv.spring.kotlin.entity.Customer
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class CustomerServiceImpl:CustomerService{
    @Autowired
    lateinit var customerDao: CustomerDao

    override fun getCustomers(): List<Customer> {
        return customerDao.getCustomers()
    }
}