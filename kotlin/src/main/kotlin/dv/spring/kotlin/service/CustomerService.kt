package dv.spring.kotlin.service

import dv.spring.kotlin.entity.Customer

interface CustomerService{
    fun getCustomers():List<Customer>
}