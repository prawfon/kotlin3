package dv.spring.kotlin.service

import dv.spring.kotlin.entity.Product

interface ProductService{
    fun getProducts():List<Product>
//    fun getProductByName(name:String): Product
}