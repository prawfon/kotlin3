package dv.spring.kotlin.dao

import dv.spring.kotlin.entity.Customer

interface CustomerDao{
    fun getCustomers():List<Customer>
}
