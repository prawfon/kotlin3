package dv.spring.kotlin.dao

import dv.spring.kotlin.entity.Product

interface ProductDao{
    fun getProducts():List<Product>
}