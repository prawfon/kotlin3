package dv.spring.kotlin.dao

import dv.spring.kotlin.entity.ShoppingCart

interface ShoppingCartDao {
    fun getShoppingCarts(): List<ShoppingCart>
}