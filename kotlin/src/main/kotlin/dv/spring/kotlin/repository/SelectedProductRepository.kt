package dv.spring.kotlin.repository

import dv.spring.kotlin.entity.SelectedProduct
import org.springframework.data.repository.CrudRepository

interface SelectedProductRepository: CrudRepository<SelectedProduct,Long>